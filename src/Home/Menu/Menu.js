import "../Menu/Menu.css";
import Name from "../../asset/img/name.png";
import Name1 from "../../asset/img/name1.png";
import Name2 from "../../asset/img/name2.png";
import Name3 from "../../asset/img/name3.png";
import Name4 from "../../asset/img/name4.png";
import Name5 from "../../asset/img/name5.png";
import Avata from "../../asset/img/ava.png";
export function Menu(props) {
  return (
    <div className="Menu">
      <div className="body-menu">
        <div className="title">
          <div className="top-title">Don't ignore your dreams</div>
          <div className="body-title">
            The 5 regrets paint a portrait of post-industrial man, who shrinks{" "}
            <br />
            himself into a shape that fits his circumstances, then turns <br />
            dutifully till he stops.
          </div>
          <div className="footer-title">
            <div className="sub-footer-title">
              <div style={{ width: "55%" }}>
                <button
                  type="button"
                  className="btn "
                  style={{ background: "#83c129", width: "90%" }}
                >
                  <div className="text-button">
                    See how it works
                  </div>
                </button>
              </div>
              <div style={{ width: "45%" }}>
                <button
                  type="button"
                  className="btn "
                  style={{ background: "#068fd5", width: "90%" }}
                >
                  <div  className="text-button">
                    Join us
                  </div>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="product">
        <div className="sub-product">
          <div className="top-product">
            <div className="sub-top-product">
              <div className="chil-sub-top-product">
                <div className="top-sub-top-product">
                  <img src={Name1} style={{ width: "100%" }} />
                </div>
                <div className="footer-sub-top-product">
                  <div className="sub-footer-sub-top-product">
                    <div className="loogo-footer-sub-top-product">
                      <img src={Avata} style={{ width: "100%" }} />
                    </div>
                    <div className="title-footer-sub-top-product">
                      <div className="sub-title-footer-sub-top-product">
                        <div className="top-sub-title-footer">
                          You neglect your friends Valerie{" "}
                        </div>
                        <div className="footer-sub-title-footer">
                          Adams. Moldova. 19.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="sub-top-product">
              <div className="chil-sub-top-product">
                <div className="top-sub-top-product">
                  <img src={Name4} style={{ width: "100%" }} />
                </div>
                <div className="footer-sub-top-product">
                  <div className="sub-footer-sub-top-product">
                    <div className="loogo-footer-sub-top-product">
                      <img src={Avata} style={{ width: "100%" }} />
                    </div>
                    <div className="title-footer-sub-top-product">
                      <div className="sub-title-footer-sub-top-product">
                        <div className="top-sub-title-footer">
                          You neglect your friends Valerie{" "}
                        </div>
                        <div className="footer-sub-title-footer">
                          Adams. Moldova. 19.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="sub-top-product">
              <div className="chil-sub-top-product">
                <div className="top-sub-top-product">
                  <img src={Name5} style={{ width: "100%" }} />
                </div>
                <div className="footer-sub-top-product">
                  <div className="sub-footer-sub-top-product">
                    <div className="loogo-footer-sub-top-product">
                      <img src={Avata} style={{ width: "100%" }} />
                    </div>
                    <div className="title-footer-sub-top-product">
                      <div className="sub-title-footer-sub-top-product">
                        <div className="top-sub-title-footer">
                          You neglect your friends Valerie{" "}
                        </div>
                        <div className="footer-sub-title-footer">
                          Adams. Moldova. 19.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="body-product">
            <div className="sub-body-product">
              <div className="chil-body-product">
                <div className="sub-chil-body-product">
                  <div className="item-chil-body-product">
                    <div className="lefp-item-chil-body-product">
                      <img src={Name3} style={{ width: "100%" }} />
                    </div>
                    <div className="right-item-chil-body-product">
                      <div className="sub-item-right">
                        <div className="top-sub-item-right">Otc 18</div>
                        <div className="bot-sub-item-right">
                          {" "}
                          I would like to avoid making these mistakes.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="sub-chil-body-product">
                  <div className="item-chil-body-product">
                    <div className="lefp-item-chil-body-product">
                      <img src={Name} style={{ width: "100%" }} />
                    </div>
                    <div className="right-item-chil-body-product">
                      <div className="sub-item-right">
                        <div className="top-sub-item-right">Otc 8</div>
                        <div className="bot-sub-item-right">
                          {" "}
                          I would like to avoid making these mistakes.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="sub-chil-body-product">
                  <div className="item-chil-body-product">
                    <div className="lefp-item-chil-body-product">
                      <img src={Name2} style={{ width: "100%" }} />
                    </div>
                    <div className="right-item-chil-body-product">
                      <div className="sub-item-right">
                        <div className="top-sub-item-right">Otc 2</div>
                        <div className="bot-sub-item-right">
                          {" "}
                          I would like to avoid making these mistakes.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <marquee direction="" onmouseover="this.stop();" onmouseout="this.start();">
          <div className="footer-product">
            <div className="sub-footer-product">
              <div className="item-footer-product">
    
                <div className="left-title">Featured on:</div>

                <div className="sub-item-footer-product sub1">
                  New Texas Time
                </div>
                <div className="sub-item-footer-product sub2">Wooden</div>
                <div className="sub-item-footer-product sub3">Vremya </div>
                <div className="sub-item-footer-product sub4">Open Book</div>
               
              </div>
            </div>
          </div>
          </marquee>
        </div>
      </div>
    </div>
  );
}

export default Menu;
