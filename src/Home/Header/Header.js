import "./Header.css";
import { showQuestion, showBag, showPerson } from "../../utils/common";
export function Header() {
  return (
   
      <div className="Header">
        <div className="body-header">
          <div className="sub-body-header">
            <div className="header-left">
              <div className="text-logo">Insight</div>
              <div className="text-right-logo">your nide slogan</div>
            </div>
            <div className="header-right">
              <div className="top-header-contact">
                <div className="icon-top-header">{showQuestion()}</div>
                <div style={{ paddingLeft: "5px" }}>How it works</div>
              </div>
              <div className="top-header-contact">
                <div className="icon-top-header">{showPerson()}</div>
                <div style={{ paddingLeft: "5px" }}>Sign up</div>
              </div>
              <div className="top-header-contact">
                <div className="icon-top-header">{showBag()}</div>
                <div style={{ paddingLeft: "5px" }}>Login</div>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
}

export default Header;
