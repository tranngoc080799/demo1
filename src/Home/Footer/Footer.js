import "./Footer.css";
import title from "../../asset/img/twitter.png";
import title1 from "../../asset/img/facebook.png";
import title2 from "../../asset/img/vimeo.png";
export function Footer() {
  return (
    <div className="Footer">
      <div className="sub-footer">
        <div className="item-footer">
          <div className="sub-item-footer">
            <div className="lefp-item">
              <div className="sub-left-item">
                <div className="title-left-item">
                  <div className="sub-title-left-item">MAIN</div>
                </div>
                <div className="chil-sub-item">
                  <span>Start Here</span>
                  <span>Portfolio</span>
                  <span> Meet Us</span>
                  <span>Blog</span>
                  <span>Contact</span>
                </div>
              </div>
              <div className="sub-center-item">
                <div className="title-left-item">
                  <div className="sub-title-left-item">COMPANY</div>
                </div>

                <div className="chil-sub-item">
                  <span>About</span>
                  <span>Help</span>
                  <span>Support</span>
                  <span>Jobs</span>
                  <span>Directory</span>
                </div>
              </div>
              <div className="sub-right-item">
                <div className="title-left-item">
                  <div className="sub-title-left-item">ONE MORE</div>{" "}
                </div>

                <div className="chil-sub-item">
                  <span>Meetups</span>
                  <span>Handbook</span>
                  <span>Privacy</span>
                  <span>API</span>
                  <span>Equipment</span>
                </div>
              </div>
            </div>
          </div>
          <div className="sub-item-footer">
            <div className="title-left-item">
              <div className="sub-title-left-item">THE LAST ONE</div>
            </div>
            <div className="chil-sub-item">
              <span>Meetups</span>
              <span>Handbook</span>
              <span>Privacy</span>
              <span>API</span>
              <span>Equipment</span>
            </div>
          </div>
          <div className="sub-item-footer">
            <div className="title-right-item">
              <div className="sub-title-right-item">
                <div className="top-sub-title-right-item">
                  <div className="img-footer">
                    <img src={title}/>
                    <img src={title1} style={{padding:'0px 10px'}}/>
                    <img src={title2}/>
                  </div>
                </div>
                <div className="footer-sub-title-right-item">
                  <div>@2013 be happy</div>
                  <div>Privacy Policy Temsort Service</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
